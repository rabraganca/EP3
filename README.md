# README

Detalhes do projeto e arquivos necessarios:

Este projeto visa o controle de membros para as empresas juniores da FGA. E uma forma de ponto
eletronico onde ficam registrados os horarios de entrada e saida dos membros que realizam 
o login no sistema.

* Ruby version 2.3.3

Para rodar o programa:

ADMIN ORCESTRA - a@a.com/12345

1- Criar um clone do projeto
2- Executar "bundle install" no console
2- Executar "rails db:create", para criar a base de dados
3- Executar "rails db:migrate", para estruturar a base de dados
4- Executar "rails db:seed", para popular a base de dados
5- Executar "rails s"
6- Abrir um browser e entrar em http://localhost:3000/

Para utilizar e testar o programa:

1- Entrar com um usuario(Admin)
2- Escolher uma empresa junior(referente ao usuario/admin)
3- Adicionar um novo membro
4- Entrar os dados do novo membro
5- Realizar o controle do novo membro atraves do e-mail e senha
6- Visualizar os registros de movimentacao dos membros que utilizam o sistema


