class OrganizationsController < ApplicationController
  def new
  end

  def create
    @organization = Organization.new(organization_params)

    if @organization.save
      redirect_to :action => 'show', :id => @organization.id
    else
      redirect_to '/'
    end
  end

  def show
    @organization = Organization.find(params[:id])
  end

  def edit
    @organization = Organization.find(params[:id])
  end

  def update
    @organization = Organization.find(params[:id])

    if @organization.update(organization_params)
      redirect_to :action => 'show', :id => @organization.id
    else
      render 'edit'
    end
  end

  def destroy
    @organization = Organization.find(params[:id])
    @organization.destroy
  end



private
  def organization_params
    params.require(:organization).permit(:name)
  end
end
