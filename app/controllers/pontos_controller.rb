class PontosController < ApplicationController
  def new
    @ponto = Ponto.new
  end

  def create
    user = User.find_by_email(params[:login][:email])
    if user && user.authenticate(params[:login][:password])
      @last_ponto = user.pontos.last

      if @last_ponto == nil
        @ponto = PontoEntrada.new(user_id: user.id)
      elsif @last_ponto.type == "PontoSaida"
        @ponto = PontoEntrada.new(user_id: user.id)
      else
        @ponto = PontoSaida.new(user_id: user.id)
      end


      if @ponto.save
        redirect_to :controller => 'organizations', :action => 'show', :id => user.organization_id
      else
        redirect_to '/'
      end
    else
      redirect_to '/'
    end
  end

  def show
    @ponto = Ponto.find(params[:id])
  end

  def edit
    @ponto = Ponto.find(params[:id])
  end

  def update
    @ponto = Ponto.find(params[:id])

    if @ponto.update(ponto_params)
      redirect_to :action => 'show', :id => @ponto.id
    else
      render 'edit'
    end
  end

  def destroy
    @ponto = Ponto.find(params[:id])
    @ponto.destroy
  end

private
  def ponto_params
    params.require(:ponto).permit(:type, :user_id)
  end
end
