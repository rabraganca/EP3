class UsersController < ApplicationController

  def new
    @org_id = params[:org_id]
    @user = User.new
  end

  def create
    @user = User.new(user_params)

    if @user.save
      Organization.find(params[:organization][:id]).users << @user
      redirect_to :action => 'show', :id => @user.id
    else
      redirect_to '/'
    end
  end

  def show
    @user = User.find(params[:id])
  end

  def edit
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])

    if @user.update(user_params)
      redirect_to :action => 'show', :id => @user.id
    else
      render 'edit'
    end
  end

  def destroy
    @user = User.find(params[:id])
    @user.delete
    redirect_to :controller => 'organizations', :action => 'show', :id => @current_user.organization_id

  end



private
  def user_params
    params.require(:user).permit(:name, :email, :password, :password_confirmation, :role)
  end
end
