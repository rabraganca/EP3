class User < ApplicationRecord
  has_secure_password

  belongs_to :organization, required: false
  has_many :pontos
end
