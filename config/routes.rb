Rails.application.routes.draw do
  root to: 'home#index'

  get '/login' => 'sessions#new'
  post '/login' => 'sessions#create'
  get '/logout' => 'sessions#destroy'

  get '/index' => 'home#index'

  resources :organizations
  resources :users

  post '/baterPonto' => 'pontos#create'
end
