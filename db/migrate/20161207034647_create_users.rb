class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users do |t|
      t.string :name
      t.string :email
      t.string :password_digest
      t.string :role, :default => "member"
      t.belongs_to :organization, index: true
      t.timestamps
    end
  end
end
