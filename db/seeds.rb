# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

orcestra = Organization.create(:name => "Orc'estra")
engrena = Organization.create(:name => "Engrena")
zenit = Organization.create(:name => "Zenit")
matriz = Organization.create(:name => "Matriz")
eletronjun = Organization.create(:name => "Eletronjun")


user = User.create(:name => "rafael", :email => "a@a.com", :password => "12345", :role => "admin")
orcestra.users << user
